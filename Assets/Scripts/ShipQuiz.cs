﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShipQuiz : MonoBehaviour
{

    public List<Ship> ships = new List<Ship>();
    private int counter = 0;
    public Image shipImageHolder;
    private Ship currentShip;
    public Text[] answers = new Text[3];
    public Text scoreBox;
    public int score = 0;
    public Button endGame;

     void Start()
    {
        if (LoadShips.Instance == null)
        {
            this.gameObject.AddComponent<LoadShips>();
            LoadShips.Instance.loadXML();
        }

        ships = LoadShips.Instance.ships;
        SelectNextShip(0);
    }

    public void SelectNextShip(int selected)
    {
        if (counter > 0) CheckAnswer(selected);
        currentShip = ships[Random.Range(0, ships.Count)];

        SetCanvasElements();

        counter++;

        if (counter == 25)
        {
            EndQuiz();
        }
    }

    private void CheckAnswer(int selected)
    {
        if (answers[selected].text.CompareTo(currentShip.getFullName()) ==0)
        {
            score++;
        }
    }

    private void SetCanvasElements()
    {
        AddRandomNamesToAnswerButtons();
        shipImageHolder.sprite = Sprite.Create(currentShip.getShipTexture(), new Rect(0, 0,
            currentShip.getShipTexture().width, currentShip.getShipTexture().height), new Vector2(.5f, .5f));
        int correctAnswer = Random.Range(0, 3);
        answers[correctAnswer].text = currentShip.getFullName();
        scoreBox.text = "Score " + score + "/" + counter + "\n (Out of 25";

    }

    private void AddRandomNamesToAnswerButtons()
    {
        foreach (Text answer in answers)
        {
            answer.text = ships[Random.Range(0, ships.Count)].getFullName();
        }
    }



    private void EndQuiz()
    {
        foreach (Text answer in answers)
        {
            answer.GetComponentInParent<Button>().enabled = false;
        }

        endGame.gameObject.SetActive(true);
    }
}