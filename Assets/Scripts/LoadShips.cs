﻿using UnityEngine;
using UnityEngine.UI;
using System.Xml;
using System.IO;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class LoadShips : Singleton<LoadShips>
{
    public List<Ship> ships = new List<Ship>();
    public Ship currentShip { get; set; }

    public override void Awake()
    {
        base.Awake();
        if (LoadShips.Instance.ships.Count == 0) loadXML();
    }

    public void loadXML()
    {
        TextAsset textData = Resources.Load<TextAsset>("Ships");
        parseShipsXML(textData.text);
    }

    private void parseShipsXML(string xmlData)
    {
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.Load(new StringReader(xmlData));

        string xmlPathPattern = "//ships/ship";
        XmlNodeList myNodeList = xmlDoc.SelectNodes(xmlPathPattern);

        foreach (XmlNode node in myNodeList)
        {
            Ship ship = new Ship(int.Parse(node.Attributes["identifier"].Value), node.Attributes["image"].Value, node.InnerText);
            ships.Add(ship);
        }
    }
}