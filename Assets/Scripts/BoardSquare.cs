﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardSquare : MonoBehaviour
{
    protected Texture2D[] images = new Texture2D[3];
    protected GameObject textBox;

    private Color color = new Color(1.0f, 0.0f, 0.0f, 0.25f);

    protected virtual void OnSquareSelected ()
    {
        textBox.GetComponent<Renderer>().enabled = true;
    }

    public virtual void ChangeSquareStatus(Color color, string str)
    {

    }

    protected virtual void UpdateEndGraphic ()
    {

    }

    public virtual void ResetSquare ()
    {
        textBox.GetComponent<Renderer>().enabled = false;
        this.GetComponent<Collider>().enabled = true;
    }

    public void ShowSquare()
    {

    }

    protected virtual void Awake ()
    {
        images[0] = Resources.Load("Square") as Texture2D;
        images[1] = Resources.Load("Square") as Texture2D;
        images[3] = Resources.Load("Square") as Texture2D;
    }

    protected virtual void Start()
    {
        this.GetComponent<Renderer>().material.mainTexture = images[1];
        textBox = this.transform.Find("TextBox").gameObject;
        textBox.GetComponent<Renderer>().enabled = false;
    }

    protected virtual void OnMouseDown ()
    {
        this.GetComponent<Renderer>().material.mainTexture = images[1];
        this.GetComponent<Collider>().enabled = false;
        OnSquareSelected();
    }
}
