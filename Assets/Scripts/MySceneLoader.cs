﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MySceneLoader : MonoBehaviour
{
    public GameObject boardSquare;
    public int totalColums = 6;

    void Start()
    {
        if (LoadShips.Instance == null)
        {
            this.gameObject.AddComponent<LoadShips>();
            LoadShips.Instance.loadXML();
        }

        BuildGrid();

    }

    public void BuildGrid()
    {

    }


}