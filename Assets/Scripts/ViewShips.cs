﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ViewShips : MonoBehaviour
{

    public GameObject boardSquare;
    public int totalColums = 3;

     void Start()
    {
        if (LoadShips.Instance == null)
        {
            this.gameObject.AddComponent<LoadShips>();
            LoadShips.Instance.loadXML();
        }

        BuildGrid();
    }

    public void BuildGrid()
    {

        Ship[] currentLayout = LoadShips.Instance.ships.ToArray();

        if (SceneManager.GetActiveScene().name == "ShipPairs")
        {
            currentLayout = GetRandomGridLayout(3, 3);
            totalColums = 3;
        }

        int rows = 0;
        int column = 0;

        for (int i = 0; i< currentLayout.Length; i++)
        {
            GameObject square = Instantiate(boardSquare) as GameObject;
            square.AddComponent<Tile>();
            square.GetComponent<Tile>().ship = currentLayout[i];
            square.transform.position = new Vector3(((-totalColumns / 2.0f) + column + .5f) * (square.GetComponent<Renderer>().bounds.size.x + .11f),
                                                     3.5f - ((square.GetComponent<Renderer>().bounds.size.y + .2f) * rows),
                                                     0);
            square.GetComponent<Collider>().enabled = true;

            column++;
            if (column == totalColums)
            {
                column = 0;
                rows++;
            }
        }
    }

    private Ship[] GetRandomGridLayout(int rows, int columns)
    {
        int tilesNeeded = rows * columns;
        Ship[] layout = new Ship[tilesNeeded];

        for (int i = 0; i < tilesNeeded; i += 2)
        {
            Ship pickShip = LoadShips.Instance.ships[Random.Range(0, FindObjectOfType<LoadShips>().ships.Count)];
            layout[i] = pickShip;
            layout[i + 1] + pickShip;
        }

        layout = Shuffle(layout);

        return layout;
    }

    private Ship[] Shuffle(Ship[] layout)
    {
        for (int i = layout.Length - 1; i > 0; i --)
        {
            int rnd = Random.Range(0, i);
            Ship temp = layout[i];
            layout[i] = layout[rnd];
            layout[rnd] = temp;
        }

        return layout;
    }

    public void ButtonEventToBuildNewGrid()
    {
        Tile[] tiles = FindObjectOfType<Tile>();
        foreach(Tile tile in tiles)
        {
            Destroy(tile.gameObject);
        }
        Invoke("BuildGrid", 0.35f);
    }


}
