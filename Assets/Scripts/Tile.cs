﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour
{
    public Ship ship;

    protected override void Start()
    {
        base.Start();

        images[1] = ship.getShipTexture();
        this.GetComponent<Renderer>().material.mainTexture = images[1];
        textBox.GetComponent<TextMesh>().text = ship.getFullName();
    }

    protected override void OnSquareSelected()
    {
        base.OnSquareSelected();

        Invoke("resetSquare", 1f);
    }

    public override void ChangeSquareStatus(Color color, string str)
    {
        base.ChangeSquareStatus(color, str);
    }

    protected override void UpdateEndGraphic()
    {
        base.UpdateEndGraphic();
    }

    public void TilePicked()
    {
        base.OnMouseDown();
    }
}